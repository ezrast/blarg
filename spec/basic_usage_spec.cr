require "./spec_helper"

# To use Blarg, first include `Blarg::Command` in the class or struct
# that will hold command line data. Then annotate each instance variable
# with either the `Flag` or `Positional` annotations.
#
# Here's a simple example:

class ClientConfig
  include Blarg::Command

  @[Flag(short: 'h')]
  getter host : String

  @[Flag(short: 'p')]
  getter port = 80

  @[Flag]
  getter timeout : Int32?

  @[Flag]
  getter ssl : Bool
end

# And here's the spec illustrating what that does:

describe ClientConfig do
  it "Handles simple arguments" do
    # Instantiate your configuration object with `.from_args`. If you
    # omit the argument, the default is `ARGV.dup`. The `.dup` is
    # important - the parser is destructive to the `Array(String)` that
    # is passed to it.
    config = ClientConfig.from_args %w(--host localhost -p 8080  --ssl --timeout 10)

    # Now we can see that the `#host`, `#port`, and `#timeout` methods
    # have been defined on ClientConfig and initialized appropriately.
    config.host.should eq "localhost"
    config.port.should eq 8080
    config.timeout.should eq 10
    config.ssl.should eq true
  end

  # Very simple arguments that only have a long form can be annotated
  # with `Flag` and no arguments.

  # In general, Blarg will throw a `Blarg::RequiredOptionError`
  # if a flag is required but not provided. A flag is considered
  # "required" if none of the following is true:
  # * The flag has a default provided, such as with `port` above.
  # * The flag's type is nilable, in which case `nil` is implicitly the
  #   default.
  # * The flag's type is Bool, in which case `false` is implicitly the
  #   default.
  it "handles missing arguments properly" do
    ClientConfig.from_args(%w(-h localhost -p 8080 --timeout 10))
      .ssl.should be_false
    ClientConfig.from_args(%w(-h localhost -p 8080 --ssl))
      .timeout.should be_nil
    ClientConfig.from_args(%w(-h localhost --timeout 10 --ssl))
      .port.should eq 80
    expect_raises(Blarg::RequiredOptionError) {
      ClientConfig.from_args %w(-p 8080 --timeout 10 --ssl) }
  end
end

# Positional arguments can be specified like so:

class ServerConfig
  include Blarg::Command

  @[Positional]
  getter host : String

  # By default, positionals are ordered the same as they are in the
  # source file. However, ordering is unspecified when annotations
  # exist in multiple source files. You can specify an ordering by
  # passing `order` to the annotation. Positionals are considered to
  # have an ordering of 0 by default, so use negative numbers for high-
  # priority positionals.
  @[Positional(order: 2)]
  # @[Positional]
  getter server_name : String?

  @[Positional(order: 1)]
  # @[Positional]
  getter port = 80

end

describe ServerConfig do
  it "takes positional arguments" do
    config = ServerConfig.from_args %w(localhost 443 test_server)
    config.host.should eq "localhost"
    config.port.should eq 443
    config.server_name.should eq "test_server"
  end

  it "uses defaults for omitted trailing arguments" do
    config = ServerConfig.from_args %w(localhost)
    config.port.should eq 80
    config.server_name.should be_nil
  end

  it "is an error to omit required positional arguments" do
    expect_raises(Blarg::RequiredOptionError) {
      config = ServerConfig.from_args %w() }
  end
end

class CopyConfig
  include Blarg::Command

  @[Flag(short: 'i')]
  getter interactive : Bool

  @[Flag(short: 'r')]
  getter recursive : Bool

  @[Flag(short: 'e')]
  getter exclude : String?

  @[Positional]
  getter src : String

  @[Positional]
  getter dest : String
end

describe CopyConfig do
  it "allows flags intermixed with positionals" do
    config1 = CopyConfig.from_args %w(foo --interactive bar)
    config1.src.should eq "foo"
    config1.dest.should eq "bar"
    config1.interactive.should be_true
    config1.recursive.should be_false
    config1.exclude.should be_nil

    config2 = CopyConfig.from_args %w(-e *.bak data archive/data -r)
    config2.src.should eq "data"
    config2.dest.should eq "archive/data"
    config2.interactive.should be_false
    config2.recursive.should be_true
    config2.exclude.should eq "*.bak"
  end
end
