require "./spec_helper"

# Flags with the `Bool` type are special in several ways, so they have
# their own page of documentation.
#
# Unlike other values, `Bool`s do not consume command line arguments for
# parsing. Instead, the presence of the flag indicates `true`, and its
# absence indicates `false`.

class BoolConfig
  include Blarg::Command

  @[Flag(short: 'f')]
  getter foo : Bool

  @[Flag]
  getter bar : Bool
end

describe BoolConfig do
  it "sets boolean values based on flag presence" do
    config = BoolConfig.from_args %w(-f)
    config.foo.should be_true
    config.bar.should be_false
  end

  # To explicitly set a `Bool` to `false`, pass `--no-{long flag}`.
  # Values get updated in sequence.
  it "can have flags negated" do
    config = BoolConfig.from_args %w(--foo --no-foo --no-bar --bar)
    config.foo.should be_false
    config.bar.should be_true
  end
end

# If you simply must buck convention, set @@blarg_negation_prefix:

class StrangeBools
  include Blarg::Command
  @@blarg_negation_prefix = "in"

  @[Flag]
  getter animate : Bool = true

  @[Flag]
  getter flammable : Bool = true
end

describe StrangeBools do
  it "replaces %s in the negation pattern with the flag name" do
    config = StrangeBools.from_args %w(--inflammable)
    config.animate.should be_true
    config.flammable.should be_false # Don't worry, it's inflammable!
  end
end
