require "./spec_helper"

# Blarg will automatically create a method called "usage" that prints
# some generated help text describing

class HelpableConfig
  include Blarg::Command
  @@blarg_name = "blarg_spec"
  @@blarg_description = "A program description"

  @[Flag]
  getter int32 : Int32

  @[Flag(description: "A string", short: 's')]
  getter string : String?

  @[Flag(short: 'b')]
  getter bool : Bool

  @[Positional]
  getter float64 : Float64

  @[Positional(description: "Another Boolean")]
  getter another_bool : Bool

  getter methods_called : Int32 = 0
  @[Flag(short: 'm')]
  def self.method_with_underscores
    @@methods_called += 1
  end

  @[Subcommand]
  getter subcommand : HelpableSubcommandA | HelpableSubcommandB | HelpableSubcommandC
end

class HelpableSubcommandA
  include Blarg::Command
  @@blarg_subname = "command-a"
  @@blarg_description = "Does command A things"

  @[Flag]
  getter amount : Int32
end

class HelpableSubcommandB
  include Blarg::Command
  @@blarg_subname = "cmd-b"
  @@blarg_description = "Does command B things"

  @[Flag]
  getter amount : Float64
end

class HelpableSubcommandC
  include Blarg::Command
  @@blarg_subname = "command-c"

  @[Flag]
  getter amount : Float64
end

describe HelpableConfig do
  it "gets a usage method automatically" do
    actual = String.build{ |ss| HelpableConfig.blarg_usage(ss) }.gsub(/\s*?\n/, "\n")
    expected = <<-HELP
      A program description
      Usage: blarg_spec [flags] <float64> [another_bool] <subcommand>

      Positionals:
        float64
        another_bool=false  Another Boolean

      Subcommands:
        command-a  Does command A things
        cmd-b      Does command B things
        command-c

      Flags:
        -b, --[no-]bool
            --int32 INT32
        -m, --method-with-underscores
        -s, --string STRING            A string
        -h, --help                     Display this help text

      HELP

    actual.should eq expected
    # actual.each_line.zip(expected.each_line) { |aa, bb| aa.should eq bb }
  end
end

class UnhelpableConfigA
  include Blarg::Command
  @@blarg_name = "abc"

  @[Flag(short: 'h')]
  getter host : String
end

describe UnhelpableConfigA do
  it "doesn't add a help flag if -h is already specified" do
    actual = String.build{ |ss| UnhelpableConfigA.blarg_usage(ss) }.gsub(/\s*?\n/, "\n")
    expected = <<-HELP
      Usage: abc [flags]

      Flags:
        -h, --host HOST

      HELP

    actual.should eq expected
  end
end

class UnhelpableConfigB
  include Blarg::Command
  @@blarg_name = "abc"

  @[Flag]
  getter help : Bool
end

describe UnhelpableConfigB do
  it "doesn't add a help flag if --help is already specified" do
    actual = String.build{ |ss| UnhelpableConfigB.blarg_usage(ss) }.gsub(/\s*?\n/, "\n")
    expected = <<-HELP
      Usage: abc [flags]

      Flags:
        --[no-]help

      HELP

    actual.should eq expected
  end
end

class ConfigWithArraysAndEnum
  include Blarg::Command
  @@blarg_name = "array"

  @[Flag]
  getter my_enum : Animal

  @[Flag]
  getter single : Int32

  @[Flag]
  getter multi : Array(Int32)

  @[Positional]
  getter my_enum_p : Animal

  @[Positional]
  getter single_p : Float64

  @[Positional]
  getter multi_p : Array(Float64)

end

describe ConfigWithArraysAndEnum do
  it "lists arrays properly" do
    actual = String.build{ |ss| ConfigWithArraysAndEnum.blarg_usage(ss) }.gsub(/\s*?\n/, "\n")
    expected = <<-HELP
      Usage: array [flags] <my_enum_p> <single_p> [multi_p...]

      Positionals:
        my_enum_p
        single_p
        multi_p=[]

      Flags:
            --multi MULTI=[]
            --my-enum MY_ENUM
            --single SINGLE
        -h, --help             Display this help text

      HELP

    actual.should eq expected
  end
end
