require "./spec_helper"

# These specs illustrate the types that can be parsed by Blarg out of
# the box. They have meaningful `from_arg` or `from_args` methods
# defined on them.

# Currently this is only `String`, all `Numeric` types, and any
# `Union`s of those types.

# `Bool` are a special case in Blarg's handling logic since they are
# not parsed from strings, and have different semantics. See
# `bool_spec.cr` for those.

# For making additional types parseable, see `custom_types_spec.cr`.
# (not yet written)

class BlargInts
  include Blarg::Command

  @[Flag]
  getter i32 : Int32

  @[Flag]
  getter i64 : Int64
end

class BlargFloats
  include Blarg::Command

  @[Flag]
  getter f32 : Float32

  @[Flag]
  getter f64 : Float64
end

class BlargStrings
  include Blarg::Command

  @[Flag]
  getter str1 : String

  @[Flag]
  getter str2 : String
end

describe Blarg do
  it "can parse numbers and strings" do
    ints = BlargInts.from_args %w(--i32 12 --i64 -300)
    floats = BlargFloats.from_args %w(--f32 7 --f64 Inf)
    strings = BlargStrings.from_args %w(--str1 apple --str2 ģȱȓ)

    ints.i32.should eq 12
    ints.i64.should eq -300
    floats.f32.should eq 7.0
    floats.f64.should eq Float64::INFINITY
    strings.str1.should eq "apple"
    strings.str2.should eq "ģȱȓ"

    ints.i64.should be_a Int64
    ints.i32.should be_a Int32
    floats.f32.should be_a Float32
    floats.f64.should be_a Float64
    strings.str1.should be_a String
    strings.str2.should be_a String
  end

  it "raises a ParseError when passed bad arguments" do
    # Five billion is too large for an i32
    expect_raises(Blarg::ParseError) do
      BlargInts.from_args %w(--i32 5000000000 --i64 -300)
    end
    # 12.0 only
    expect_raises(Blarg::ParseError) do
      BlargInts.from_args %w(--i32 12 --i64 12.0)
    end

    # horse definitely isn't a float
    expect_raises(Blarg::ParseError) do
      BlargFloats.from_args %w(--f32 horse --f64 Inf)
    end
    # neither is the empty string
    expect_raises(Blarg::ParseError) do
      BlargFloats.from_args ["--f32", "7", "--f64", ""]
    end

    # Strings cannot fail to parse, so no such spec:
    # expect_raises(Blarg::ParseError) do
    #   BlargStrings.from_args %w(--str1 ??? --str2 ???)
    # end

  end
end

class BlargUnions
  include Blarg::Command

  @[Flag]
  getter alpha : Int32 | Float64 | Nil

  @[Flag]
  getter bravo : Float64 | Int32
end

describe Blarg do
  it "checks types in arbitrary order, ignoring Nil" do
    args = BlargUnions.from_args %w(--alpha 1 --bravo 2)
    # Note that alpha is not an Int32 as you might expect.
    # Union type ordering is an implementation detail and probably
    # should not be relied upon.
    args.alpha.should be_a Float64
    args.bravo.should be_a Float64
  end

  it "treats nilable values as optional" do
    args = BlargUnions.from_args %w(--bravo 0)
    args.alpha.should be_nil
    args.bravo.should be_a Float64
  end
end

class BlargEnums
  include Blarg::Command

  @[Flag]
  getter enum1 : Animal = Animal::Giraffe

  @[Positional]
  getter enum2 : Animal
end

describe BlargEnums do
  it "parses enum values" do
    args = BlargEnums.from_args %w(--enum1 marmoset quokka)
    args.enum1.should eq Animal::Marmoset
    args.enum2.should eq Animal::Quokka
  end

  it "uses defaults" do
    args = BlargEnums.from_args %w(quokka)
    args.enum1.should eq Animal::Giraffe
    args.enum2.should eq Animal::Quokka
  end

  it "rejects invalid values" do
    expect_raises(Blarg::ParseError) do
      args = BlargEnums.from_args %w(1)
    end
  end
end
