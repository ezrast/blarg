require "./spec_helper"

# Blarg allows you to specify subcommands similar to those used by
# software such as git and docker. A subcommand is a special type of
# positional argument that consumes the entire remainder of the command
# line to build a different type of object. For example, you may wish
# to take different sets of options based on whether a user is listing
# files or reading them:
class DataStoreConfig
  include Blarg::Command

  @[Flag]
  getter tag : String?

  @[Flag]
  getter anonymous : Bool

  @[Subcommand]
  getter subcommand : ListConfig | ReadConfig
end

# On each of the types that a subcommand can take, you should specify
# `@@blarg_subname`; this is what the user will have to enter on the
# command line to tell Blarg to start parsing this type of object.
class ListConfig
  include Blarg::Command
  @@blarg_subname = "list"

  @[Flag]
  getter show_hidden : Bool

  @[Flag]
  getter tag : String?
end

class ReadConfig
  include Blarg::Command
  @@blarg_subname = "read"

  @[Flag]
  getter path : String

  @[Flag]
  getter max_bytes : Int32
end

describe DataStoreConfig do
  it "returns different objects based on the subcommand" do
    config = DataStoreConfig.from_args %w(list --show-hidden)
    sub = config.subcommand
    sub.should be_a ListConfig
    sub.as(ListConfig).show_hidden.should be_true

    config = DataStoreConfig.from_args %w(read --path=myfile.txt --max-bytes=1024)
    sub = config.subcommand
    sub.should be_a ReadConfig
    sub = sub.as(ReadConfig)
    sub.path.should eq "myfile.txt"
    sub.max_bytes.should eq 1024
  end

  it "is an error to pass invalid subcommand arguments" do
    begin
      DataStoreConfig.from_args %w(list --anonymous)
    rescue err : Blarg::BlargException
      error = err
    end
    error.should be_a Blarg::SubcommandError
    error = error.as Blarg::SubcommandError
    error.inner.should be_a Blarg::UnknownArgumentError
  end

  it "attaches flags to the correct object" do
    config = DataStoreConfig.from_args %w(list --tag=abc)
    config.tag.should be_nil
    config.subcommand.as(ListConfig).tag.should eq "abc"

    config = DataStoreConfig.from_args %w(--tag=abc list)
    config.tag.should eq "abc"
    config.subcommand.as(ListConfig).tag.should be_nil
  end

  it "raises when no subcommand is provided" do
    expect_raises(Blarg::RequiredSubcommandError) do
      config = DataStoreConfig.from_args %w()
    end
  end

  it "finds errors in the main command first" do
    expect_raises(Blarg::UnknownArgumentError) do
      # would otherwise raise RequiredOptionError
      DataStoreConfig.from_args %w(blah read)
    end
  end
end
