require "./spec_helper"

# By default, Blarg uses the somewhat-standard GNU style for options.
# Booleans are `--foo` or `-f`. Flags with arguments are `--foo bar` or
# `--foo=bar` or `-fbar`. `-fg` is equivalent to either `--foo g` or
# `-f -g` depending on whether `foo` takes an argument.
#
# Some of these conventions can be changed if you prefer a different
# style. Setting `@@blarg_long_prefix`, `@@blarg_short_prefix`, and
# `@@blarg_assignment` class variables lets you change the strings Blarg
# uses in place of `--`, `-`, and `=`. `@@blarg_short_prefix` and
# `@@blarg_assignment` can be set to `nil` to disable those features
# entirely.
#
# For example, one may wish for flags to begin with `/`, never combine,
# and use `:` for assignment, like so:

class RedmondStyle
  include Blarg::Command
  @@blarg_long_prefix = "/"
  @@blarg_short_prefix = nil
  @@blarg_assignment = ":"

  @[Flag(long: "p")]
  getter profile : String?

  @[Flag(long: "?")]
  getter help : Bool

  @[Positional]
  getter cmd : String?
end

describe RedmondStyle do
  it "uses `/` for flags" do
    config = RedmondStyle.from_args %w(/p:user1 notepad.exe /?)
    config.profile.should eq "user1"
    config.cmd.should eq "notepad.exe"
    config.help.should be_true
  end

  it "treats `--` args as positional" do
    config = RedmondStyle.from_args %w(--c)
    config.cmd.should eq "--c"
  end

  it "treats `-` args as positional" do
    config = RedmondStyle.from_args %w(-c)
    config.cmd.should eq "-c"
  end

  it "does not work with `=`" do
    expect_raises(Blarg::UnknownArgumentError) do
      RedmondStyle.from_args ["/p=foo"]
    end
  end
end

# Or perhaps you want a limited set of more natural commands with no
# prefix, and use `+` for short options for some reason:

class NaturalStyle
  include Blarg::Command
  @@blarg_long_prefix = ""
  @@blarg_short_prefix = "+"
  @@blarg_assignment = nil

  @[Flag(short: 'f')]
  getter from : String?

  @[Flag(short: 't')]
  getter to : String

  @[Flag(short: 'u')]
  getter urgent : Bool

  @[Flag(short: 's')]
  getter save_sent : Bool
end

describe NaturalStyle do
  it "Parses long and short options correctly" do
    messenger = NaturalStyle.from_args %w(+sf Bob to Alice urgent)
    messenger.from.should eq "Bob"
    messenger.to.should eq "Alice"
    messenger.urgent.should be_true
    messenger.save_sent.should be_true
  end

  it "Disallows assignment with `=`" do
    expect_raises(Blarg::UnknownArgumentError) do
      NaturalStyle.from_args %w(from=Accounting)
    end
  end
end

# There is one other "convention" class variable, named
# `negation_prefix`. It is documented in `bool_spec.cr`.
