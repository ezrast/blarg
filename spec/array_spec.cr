require "./spec_helper"

class VariadicCmd
  include Blarg::Command

  @[Flag]
  getter exclude : Array(String | Int32)

  @[Positional]
  getter file_names : Array(String | Float32)
end

describe VariadicCmd do
  it "parses multiple flags and positionals" do
    cmd = VariadicCmd.from_args %w(--exclude duck sheep --exclude 123 456.0)
    cmd.exclude.should eq ["duck", 123]
    cmd.file_names.should eq ["sheep", 456.0]
  end

  it "allows empty arrays" do
    cmd = VariadicCmd.from_args %w()
    cmd.exclude.should be_empty
    cmd.file_names.should be_empty
  end
end
