require "./annotations"
require "./class_methods"
require "./exceptions"
require "./primitives"

module Blarg::Command
  def __blarg_negation(long : String)
    return nil unless negation_prefix = @@blarg_negation_prefix
    return negation_prefix + long
  end

  def initialize(*, __blarg_argv arr : Array(String))
    {% begin %}
      {% flags = [] of Nil %}
      {% positionals = [] of Nil %}
      {% subcommand = nil %}

      {% for ivar in @type.instance_vars %}
        {% if flag_ann = ivar.annotation(Annotations::Flag) %}
          {%
            flag = {
              attribute: ivar.name,
              type: ivar.type,
              long: flag_ann[:long] || ivar.name.stringify.tr("_", "-"),
              short: flag_ann[:short],
              default: ivar.default_value || (
                ivar.type <= Bool ? "false".id :
                ivar.type <= Array ? "(#{ivar.type}.new)".id : # different from class_methods.cr
                nil
              ),
              doc_name: flag_ann[:doc_name] || ivar.name.upcase.stringify,
              description: flag_ann[:description] || ""
            }
          %}
          {% unless flag[:short].is_a? CharLiteral || flag[:short].is_a? NilLiteral %}
            {% raise "Short flag must be a Char for flag ${attribute}. Got: #{flag[:short]}" %}
          {% end %}

          {% flags << flag %}
        {% end %}
        {% if pos_ann = ivar.annotation(Annotations::Positional) %}
          {%
            pos = {
              attribute: ivar.name,
              type: ivar.type,
              default: ivar.default_value || (
                ivar.type == Bool ? "false".id :
                ivar.type <= Array ? "(#{ivar.type}.new)".id : # different from class_methods.cr
                nil
              ),
              order: pos_ann[:order] || 0,
              doc_name: pos_ann[:doc_name] || ivar.name.stringify,
              description: pos_ann[:description],
              ast_node: pos_ann,
            }
          %}

          {% positionals << pos %}
        {% end %}

        {% if subcmd_ann = ivar.annotation(Annotations::Subcommand) %}
          {% if subcommand %}
            {% raise "A command may have only one set of subcommands: #{subcommand[:ast_node]}, #{subcmd_ann}" %}
          {% end %}
          {%
            subcommand = {
              attribute: ivar.name,
              type: ivar.type,
              ast_node: subcmd_ann,
              doc_name: subcmd_ann[:doc_name] || ivar.name.stringify,
            }
          %}
        {% end %}
      {% end %}

      {%
        positionals = positionals.sort_by{ |pos| pos[:ast_node].column_number }
          .sort_by{ |pos| pos[:ast_node].line_number }
          .sort_by{ |pos| pos[:ast_node].filename }
          .sort_by{ |pos| pos[:order] }
      %}

      {% for method in @type.class.methods %}
        {% if meth_ann = method.annotation(Annotations::Flag) %}
          {%
            flags << {
              method: method.name,
              long: meth_ann[:long] || method.name.stringify.tr("_", "-"),
              short: meth_ann[:short],
              description: meth_ann[:description],
            }
          %}
        {% end %}
      {% end %}

      {%
        flags = flags.sort_by{ |flag| flag[:long] }
      %}

      {% unless flags.any? { |flag| flag[:short] == 'h' || flag[:long] == "help"} %}
        {%
          flags << {
            method: :blarg_usage_and_exit.id,
            long: "help",
            short: 'h',
            description: "Display this help text"
          }
        %}
      {% end %}



      #######################
      # Codegen starts here #
      #######################
      {% for opt in positionals + flags.reject{ |fl| fl[:method] } %}
        %placeholder{opt[:attribute]} = {{ opt[:default] }}
      {% end %}

      long_prefix = @@blarg_long_prefix
      short_prefix = {% if flags.any?{ |fl| fl[:short] } %} @@blarg_short_prefix {% else %} nil {% end %}
      assignment = @@blarg_assignment

      leftovers = [] of String

      while arg = arr.shift?
        case
        {% for flag in flags %}
          # Long flags
          when arg == long_prefix + {{ flag[:long] }}
            {% if flag[:method] %}
                self.class.{{ flag[:method] }}
            {% elsif flag[:type] <= Bool %}
              %placeholder{flag[:attribute]} = true
              when (neg = __blarg_negation({{ flag[:long] }})) && arg == long_prefix + neg
                %placeholder{flag[:attribute]} = false
              # endwhen
            {% else %}
              raise Blarg::InsufficientArgumentsError.new({{ flag[:type] }}, {{ flag[:long] }}) unless val = arr.shift?
              {% if flag[:type] <= Array %}
                %placeholder{flag[:attribute]} << ({{ flag[:type].type_vars.first.id }}).from_arg(val)
              {% else %}
                %placeholder{flag[:attribute]} = ({{ flag[:type] }}).from_arg(val)
              {% end %}
            # endwhen
            when assignment && (arg.starts_with? long_prefix + {{ flag[:long] }} + assignment)
              val = arg.lchop(long_prefix + {{ flag[:long] }} + assignment.as(String))
              {% if flag[:type] <= Array %}
                %placeholder{flag[:attribute]} << ({{ flag[:type].type_vars.first.id }}).from_arg(val)
              {% else %}
                %placeholder{flag[:attribute]} = ({{ flag[:type] }}).from_arg(val)
              {% end %}
            {% end %}
          # endwhen
        {% end %}
        when !long_prefix.empty? && arg.starts_with? long_prefix # error is nicer if we don't let --badarg fall through to short flags
          raise Blarg::UnknownArgumentError.new(arg) unless long_prefix.empty?
        # endwhen
        # Short flags
        when short_prefix && arg.starts_with? short_prefix
          short_prefix = short_prefix.as(String)
          short_args = arg.lchop(short_prefix).chars
          while short_arg = short_args.shift?
            case short_arg
            {% for flag in flags.select{ |fl| fl[:short] } %}
              when {{ flag[:short] }}
                {% if flag[:method] %}
                  self.class.{{ flag[:method] }}
                {% elsif flag[:type] <= Bool %}
                  %placeholder{flag[:attribute]} = true
                {% else %}
                  val = short_args.join
                  val = arr.shift? if val.empty?
                  raise Blarg::InsufficientArgumentsError.new({{ flag[:type] }}, {{ flag[:long] }}) unless val

                  {% if flag[:type] <= Array %}
                    %placeholder{flag[:attribute]} << ({{ flag[:type].type_vars.first.id }}).from_arg(val)
                  {% else %}
                    %placeholder{flag[:attribute]} = ({{ flag[:type] }}).from_arg(val)
                  {% end %}
                  short_args.clear
                {% end %}
              # endwhen
            {% end %}
            else
              raise Blarg::UnknownArgumentError.new(short_prefix + short_arg)
            end
          end
        # endwhen
        {% if subcommand %}
          {% for klass in subcommand[:type].union_types %}
            when arg == {{ klass }}.blarg_subname
              %subcommand_info = {
                klass: {{ klass }},
                args: arr
              }
              arr = [] of String
          {% end %}
        {% end %}
        else
          leftovers << arg
        end
      end

      {% for pos in positionals %}
        {% if pos[:type] <= Array %}
          %placeholder{pos[:attribute]}.concat leftovers.map{ |item| ({{ pos[:type].type_vars.first.id }}).from_arg(item) }
          leftovers.clear
        {% else %}
          if arg = leftovers.shift?
            %placeholder{pos[:attribute]} = ({{ pos[:type] }}).from_arg(arg)
          end
        {% end %}
      {% end %}
      raise Blarg::UnknownArgumentError.new(leftovers.first) unless leftovers.empty?

      {% for opt in positionals + flags.reject{ |fl| fl[:method] } %}
        unless %placeholder{opt[:attribute]}.is_a? ({{ opt[:type] }})
          raise Blarg::RequiredOptionError.new({{ opt[:long] || opt[:attribute].stringify }})
        end
        @{{ opt[:attribute] }} = %placeholder{opt[:attribute]}
      {% end %}

      {% if subcommand %}
        if %subcommand_info
          @{{ subcommand[:attribute] }} = %subcommand_info[:klass].__blarg_subcommand_from_args(%subcommand_info[:args])
        else
          raise Blarg::RequiredSubcommandError.new
        end
      {% end %}
    {% end %}
  end

  macro included
    extend ::Blarg::Command::ClassMethods
    class_property blarg_subname : String = self.name
    class_property blarg_long_prefix : String = "--"
    class_property blarg_short_prefix : String? = "-"
    class_property blarg_negation_prefix : String? = "no-"
    class_property blarg_assignment : String? = "="
    class_property blarg_usage_method : Symbol? = :usage
    class_property blarg_name : String = PROGRAM_NAME
    class_property blarg_description : String? = nil

    class SubcommandError < Blarg::SubcommandError
      def parent_class
        {{ @type }}
      end
    end

    def self.__blarg_subcommand_from_args(argv : Array(String))
      from_args(argv)
    rescue err : Blarg::BlargException
      raise SubcommandError.new(err)
    end
  end
end
