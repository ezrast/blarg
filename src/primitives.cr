def Object.from_arg(str : String)
  raise Blarg::ParseError.new(self, str)
end

def Number.from_arg(str : String)
  new(str)
rescue err
  raise Blarg::ParseError.new(self, str, err)
end

def String.from_arg(str : String)
  str
end

def Union.from_arg(str : String)
  {% for type in T.reject{|ty| ty == Nil} %}
    begin
      return ({{ type }}).from_arg(str)
    rescue Blarg::BlargException
    end
  {% end %}
  raise Blarg::ParseError.new(self, str)
end

def Enum.from_arg(str : String)
  self.parse(str)
rescue err : ArgumentError
  raise Blarg::ParseError.new(self, str, err)
end
