module Blarg
  class BlargException < Exception
  end

  class UnknownArgumentError < BlargException
    getter argument : String

    def initialize(@argument)
      super "Unknown argument: #{argument}"
    end
  end

  class RequiredOptionError < BlargException
    getter option_name : String

    def initialize(@option_name)
      super "Option #{option_name} is required but not provided"
    end
  end

  class RequiredSubcommandError < BlargException
    def initialize
      super "Subcommand is required but not provided"
    end
  end

  abstract class SubcommandError < BlargException
    getter inner : BlargException

    abstract def parent_class

    def initialize(@inner)
      super "Error parsing subcommand #{parent_class.blarg_subname}: #{inner}"
    end
  end

  class ParseError < BlargException
    getter class_name : String
    getter str : String

    def initialize(klass, @str, cause : Exception? = nil)
      @class_name = klass.to_s
      super "Could not parse #{class_name} from string: #{str}", cause
    end
  end

  class InsufficientArgumentsError < BlargException
    getter class_name : String
    getter option_name : String?

    def initialize(klass, @option_name = nil)
      @class_name = klass.to_s
      if option_name
        super "Not enough arguments provided to parse #{class_name} for option #{option_name}"
      else
        super "Not enough arguments provided to parse #{class_name}"
      end
    end
  end
end
