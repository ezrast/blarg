module Blarg::Command::ClassMethods
  def from_args(argv : Array(String) = ARGV.dup)
    new(__blarg_argv: argv)
  end

  def from_args!(argv : Array(String) = ARGV.dup)
    new(__blarg_argv: argv)
  rescue err : SubcommandError
    while (inner = err.inner).is_a? SubcommandError
      err = inner
    end
    err.parent_class.blarg_usage_and_exit(STDERR, 1)
  rescue err : BlargException
    STDERR.puts "Error parsing commands: #{err}"
    blarg_usage_and_exit(STDERR, 1)
  end

  def blarg_usage_and_exit(io : IO = STDOUT, exit_code : Int32 = 0)
    blarg_usage(io)
    exit exit_code
  end

  def blarg_usage(io : IO = STDOUT)
    {% begin %}
      {% flags = [] of Nil %}
      {% positionals = [] of Nil %}
      {% subcommand = nil %}

      {% for ivar in @type.instance_vars %}
        {% if flag_ann = ivar.annotation(Annotations::Flag) %}
          {%
            flag = {
              attribute: ivar.name,
              type: ivar.type,
              long: flag_ann[:long] || ivar.name.stringify.tr("_", "-"),
              short: flag_ann[:short],
              default: ivar.default_value || (
                ivar.type <= Bool ? "false".id :
                ivar.type <= Array ? "[]".id : # different from command.cr
                nil
              ),
              doc_name: flag_ann[:doc_name] || ivar.name.upcase.stringify,
              description: flag_ann[:description] || ""
            }
          %}
          {% unless flag[:short].is_a? CharLiteral || flag[:short].is_a? NilLiteral %}
            {% raise "Short flag must be a Char for flag ${attribute}. Got: #{flag[:short]}" %}
          {% end %}

          {% flags << flag %}
        {% end %}

        {% if pos_ann = ivar.annotation(Annotations::Positional) %}
          {%
            pos = {
              attribute: ivar.name,
              type: ivar.type,
              default: ivar.default_value || (
                ivar.type == Bool ? "false".id :
                ivar.type <= Array ? "[]".id : # different from command.cr
                nil
              ),
              order: pos_ann[:order] || 0,
              doc_name: pos_ann[:doc_name] || ivar.name.stringify,
              description: pos_ann[:description],
              ast_node: pos_ann,
            }
          %}

          {% positionals << pos %}
        {% end %}

        {% if subcmd_ann = ivar.annotation(Annotations::Subcommand) %}
          {% if subcommand %}
            {% raise "A command may have only one set of subcommands: #{subcommand[:ast_node]}, #{subcmd_ann}" %}
          {% end %}
          {%
            subcommand = {
              attribute: ivar.name,
              type: ivar.type,
              ast_node: subcmd_ann,
              doc_name: subcmd_ann[:doc_name] || ivar.name.stringify,
            }
          %}
        {% end %}
      {% end %}

      {%
        positionals = positionals.sort_by{ |pos| pos[:ast_node].column_number }
          .sort_by{ |pos| pos[:ast_node].line_number }
          .sort_by{ |pos| pos[:ast_node].filename }
          .sort_by{ |pos| pos[:order] }
      %}

      {% for method in @type.class.methods %}
        {% if meth_ann = method.annotation(Annotations::Flag) %}
          {%
            flags << {
              method: method.name,
              long: meth_ann[:long] || method.name.stringify.tr("_", "-"),
              short: meth_ann[:short],
              description: meth_ann[:description],
            }
          %}
        {% end %}
      {% end %}

      {%
        flags = flags.sort_by{ |flag| flag[:long] }
      %}

      {% unless flags.any? { |flag| flag[:short] == 'h' || flag[:long] == "help"} %}
        {%
          flags << {
            method: :blarg_usage_and_exit.id,
            long: "help",
            short: 'h',
            description: "Display this help text"
          }
        %}
      {% end %}

      ###########
      # Codegen #
      ###########
      long_prefix = @@blarg_long_prefix
      short_prefix = {% if flags.any?{ |fl| fl[:short] } %} @@blarg_short_prefix {% else %} nil {% end %}
      assignment = @@blarg_assignment

      name = @@blarg_name
      description = @@blarg_description
      negation_prefix = @@blarg_negation_prefix

      if description
        io << description << "\n"
      end
      io << "Usage: " << name
      {% unless flags.empty? %}
        io << " [flags]"
      {% end %}
      {% for pos in positionals %}
        {% if pos[:type] <= Array %}
          io << {{ " [" + pos[:doc_name] + "...]" }}
        {% elsif pos[:type] <= Bool || pos[:type].nilable? %}
          io << {{ " [" + pos[:doc_name] + "]" }}
        {% else %}
          io << {{ " <" + pos[:doc_name] + ">" }}
        {% end %}
      {% end %}
      {% if subcommand %}
        io << {{ " <" + subcommand[:doc_name] + ">"}}
      {% end %}

      {% unless positionals.empty? %}
        io << "\n\nPositionals:"
        {% for pos in positionals %}
          io << {{ "\n  " + pos[:doc_name] }}
          {% unless pos[:default] == nil %}
            io << {{ "=" + pos[:default].stringify }}
          {% end %}
          {% if pos[:description] %}
            io << {{ "  " + pos[:description] }}
          {% end %}
        {% end %}
      {% end %}

      {% if subcommand %}
      column_width = 0
        {% for klass in subcommand[:type].union_types %}
          column_width = {column_width, {{ klass }}.blarg_subname.size}.max
        {% end %}
        io << "\n\nSubcommands:"
        {% for klass in subcommand[:type].union_types %}
          io << "\n  " << {{ klass }}.blarg_subname
          (column_width - {{ klass }}.blarg_subname.size).times do
            io << " "
          end
          io << "  " << {{ klass }}.blarg_description
        {% end %}
      {% end %}

      io << "\n\nFlags:\n"
      column_width = 0
      {% for flag in flags %}
        %column{flag} = String.build do |str|
          if short_prefix
            {% if flag[:short] %}
              str << "  " << short_prefix << {{ (flag[:short].id + ", ").stringify }} << long_prefix
            {% else %}
              str << "  " << short_prefix.gsub(/./, " ") << "   " << long_prefix
            {% end %}
          else
            str << "  " << long_prefix
          end

          {% if flag[:method] %}
            str << {{ flag[:long] }}
          {% elsif flag[:type] <= Bool %}
            if negation_prefix
              str << "[" << negation_prefix << "]"
            end
            str << {{ flag[:long] }}
          {% else %}
            str << {{ flag[:long] + " " + flag[:doc_name] }}
            {% unless flag[:default] == nil %}
              str << {{ "=" + flag[:default].stringify }}
            {% end %}
          {% end %}
        end
        column_width = {column_width, %column{flag}.size}.max
      {% end %}

      {% for flag in flags %}
        io << %column{flag}
        (column_width - %column{flag}.size).times do
          io << " "
        end
        {% if flag[:description] %}
          io << {{ "  " + flag[:description] }}
        {% end %}
        io << "\n"
      {% end %}
    {% end %}
  end
end
