## 0.2.4 ~ 2021-05-05
* Update Crystal version

## 0.2.3 ~ 2020-07-04
* Justify subcommand columns in usage text
* Replace _ (underscore) with - (hyphen) in default long arguments
* Errors in main command arguments are now raised before subcommand is parsed
* Errors in subcommands now properly cause the relevant usage text to be shown

## 0.2.2 ~ 2019-09-03
* Add support for enums

## 0.2.1 ~ 2019-09-01
* Positionals take an `order` parameter
* `blarg_usage` and `from_args` no longer exit the program
* Old `blarg_usage` is now `blarg_usage_and_exit` and old `from_args` is now `from_args!`
* Flags are ordered alphabetically
* Arrays are better represented in usage text

## 0.2.0 ~ 2019-08-31
* Added a changelog
