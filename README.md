# Blarg: a declarative argument parser with familiar syntax

Blarg is yet another argument parsing library for the Crystal language.

Crystal's `OptionParser` is nice, but I find the syntax unintuitive and
magical, and passing in a block for every argument is tedious when what
I want is to just assign everything to a global config object and deal
with the particulars later.

Blarg is inspired by the `JSON::Serializable` module. Blarg uses
annotations to define a constructor that builds your object from
command-line arguments. Then you just call the constructor and go on
your merry way, with no need to manually instantiate a parser and no
special semantics on the object you get back.

Blarg supports `String`s, `Bool`s, and `Number`s, and adding more types
is as simple as defining `.from_arg(str)` on the class in question. All
common flag styles are supported - the following are equivalent:
  * `--quiet --foo bar`
  * `-q --foo=bar`
  * `-q -f=bar`
  * `-qfbar`

## Installation

Add this to your application's `shard.yml`:

```yaml
dependencies:
  blarg:
    gitlab: ezrast/blarg
```

## Usage

Full documentation is not yet complete, but there's a simple example
here, and some more detailed info is written into the spec files (start
with `basic_usage_spec.cr`).

```crystal
require "../src/blarg"

class Config
  include Blarg::Command

  @[Flag(short: 'n')]
  getter name : String?

  @[Flag]
  getter age : Int32?

  @[Flag]
  getter species : String = "human"

end

begin
  config = Config.from_args()
rescue err : Blarg::BlargException
  STDERR.puts "Error: #{err}"
  Config.blarg_usage(STDERR, 1) # Print usage and exit with code 1
  exit
end

puts "Your name is #{config.name},"
if age = config.age
  puts "you are #{age} years old,"
else
  puts "I don't know your age,"
end
puts "and you are a #{config.species}."
```

## Contributing

1. Fork it
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create a new Merge Request

## Contributors

- [[ezrast]](https://gitlab.com/ezrast) Ezra Stevens - creator, maintainer
